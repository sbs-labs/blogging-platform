# Blogging platform

any automation needed to maintain blogging platform

## TODO
- [x] Install Helm in docker image
- [x] Cert Manager install script
- [ ] Cert Manager issuers
- [ ] Traefik install script
- [ ] Install Ghost
- [ ] Migrate this to Kubernetes bootstrap project
- [ ] Use Kubernetes bootstrap to keep cluster updated
- [ ] Install Falco + rulesets
- [ ] Image scanning in build process

## Components

### Ingress
https://hub.helm.sh/charts/stable/traefik

### Certificates
https://hub.helm.sh/charts/jetstack/cert-manager

#### State
https://github.com/helm/charts/tree/master/stable/mariadb

### Ghost
https://hub.helm.sh/charts/bitnami/ghost

## Build/Setup

### Secrets
https://docs.gitlab.com/ee/ci/variables/

#### `KUBERNETES_CONFIG`

The kubernetes configuration file, this will authorize the gitlab runner to perform tasks against the kubernetes API.

To setup this variable you will need to have already obtained a kubernetes configuration file. Copy the contents of that file, and paste them into the secret value, make sure to set the secret to a [`file` type secret](https://docs.gitlab.com/ee/ci/variables/#file-type)

```bash
KUBERNETES_CONFIG=<contents_of_config_file>
```

#### `CERT_MANAGER_VALUES`

To set the [configuration options](https://hub.helm.sh/charts/jetstack/cert-manager/v0.12.0) for `cert-manager` you must create a [`file` type secret](https://docs.gitlab.com/ee/ci/variables/#file-type) in gitlab. The contents, "value" should be the YAML content that would normally exist in the values file. This isn't great for debugging, or knowing what the values are at any given time, but it will get the job done for now.

## Debug

### Scripts

#### install_cert_manager

```
# required variables
CERT_MANAGER_RELEASES=https://api.github.com/repos/jetstack/cert-manager/releases \
CERT_MANAGER_CRDS=https://raw.githubusercontent.com/jetstack/cert-manager/\<CERT_MANAGER_COMMIT\>/deploy/manifests/00-crds.yaml \
# needed if testing custom values during helm install, omit to use defaults
CERT_MANAGER_VALUES=values.yaml \
./bin/install_cert_manager
```

### Build

first build the docker build image, to be used in the gitlab local jobs, and run a local registry

```bash
docker build -t localhost:5000/sbs-labs/build-image -f Dockerfile.build ./
docker run -d -p 5000:5000 --restart=always --name registry registry:2
docker push localhost:5000/sbs-labs/build-image
```

#### Stage: "build" Job runs

##### Docker Build Image

to debug just run the `docker build ...` command above. This obviously only tests the docker image build. Hopefully the registry is still good, and functional :-). It is hard to do the login process with the local registry, I don't generally create users.

#### Stage: "Prep" Job runs

##### Cert-Manager

Make sure you've built the docker image above, and that it is locally available

```bash
gitlab-runner exec docker \
--env CI_REGISTRY_IMAGE=localhost:5000/sbs-labs/build-image \
--env KUBERNETES_CONFIG="$(kubectl config view --raw)" \
"Cert Manager"
```
